package com.example.pulco.Resources;

import com.example.pulco.Model.User;
import com.example.pulco.Repository.UserRepository;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/test")
public class TestResource {

    @Inject
    UserRepository userRepository;

    @GET
    @Path("/create_user")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createDefaultUser(){
        User defaultUser = new User("bot", "default", "default.accout@email.com", "123456");
        if(userRepository.add(defaultUser)){
            return Response.ok(defaultUser).build();
        }

        return Response.ok("Default user already exist").build();
    }

    @GET
    @Path("/")
    public Response test(){
        return Response.ok("All good").build();
    }

}
